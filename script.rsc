# aug/30/2021 15:55:19 by RouterOS 6.48.3
# software id = 5FV8-1P9I
#
# model = RB924i-2nD-BT5&BG77

/system script
add dont-require-permissions=no name=get_tags owner=admin policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon source=":\
    global crc8\r\
    \n:global revb\r\
    \n:global sb\r\
    \n\r\
    \n:if ([:len \$crc8] = 0) do={\r\
    \n:put (\"[*] Loading utils...\")\r\
    \n/system script run utils\r\
    \n}\r\
    \n\r\
    \n\r\
    \n# Required packages: iot\r\
    \n\r\
    \n################################ Configuration #########################\
    #######\r\
    \n\r\
    \n# Interface whose MAC should be used as 'Locator ID'\r\
    \n:local locatorIface \"ether1\"\r\
    \n\r\
    \n# POSIX regex for filtering advertisement Bluetooth addresses. E.g. \"^B\
    C:33:AC\"\r\
    \n# would only include addresses which start with those 3 octets.\r\
    \n# To disable this filter, set it to \"\"\r\
    \n:local addressRegex \"\"\r\
    \n\r\
    \n# POSIX regex for filtering Bluetooth advertisements based on their data\
    . Same\r\
    \n# usage as with 'addressRegex'.\r\
    \n:local advertisingDataRegex \"^0201061affffff54\"\r\
    \n\r\
    \n#################################### System ############################\
    #######\r\
    \n:put (\"[*] Gathering system info...\")\r\
    \n:local ifaceMac [/interface get [/interface find name=\$locatorIface] ma\
    c-address]\r\
    \n:local cpuLoad [/system resource get cpu-load]\r\
    \n:local freeMemory [/system resource get free-memory]\r\
    \n:local usedMemory ([/system resource get total-memory] - \$freeMemory)\r\
    \n:local rosVersion [/system package get value-name=version \\\r\
    \n    [/system package find where name ~ \"^routeros\"]]\r\
    \n:local model [/system routerboard get value-name=model]\r\
    \n:local serialNumber [/system routerboard get value-name=serial-number]\r\
    \n# Health is a bit iffy since '/system health' does not have 'find' in RO\
    S6\r\
    \n:local health [/system health print as-value]\r\
    \n:local supplyVoltage 0\r\
    \n:local boardTemp 0\r\
    \n:foreach k,v in=\$health do={\r\
    \n    :if (\$k = \"voltage\") do={:set \$supplyVoltage \$v}\r\
    \n    :if (\$k = \"board-temperature1\") do={:set \$boardTemp \$v}\r\
    \n}\r\
    \n\r\
    \n################################## Bluetooth ###########################\
    #######\r\
    \n:put (\"[*] Gathering Bluetooth info...\")\r\
    \n:global btOldestAdvertisementTimestamp\r\
    \n:if ([:typeof \$btOldestAdvertisementTimestamp] = \"nothing\") do={\r\
    \n    # First time this script has been run since booting, need to initial\
    ize\r\
    \n    # persistent variables\r\
    \n    :set \$btOldestAdvertisementTimestamp 0\r\
    \n}\r\
    \n:local btProcessingStart [/system clock get time]\r\
    \n:local advertisements [/iot bluetooth scanners advertisements print deta\
    il \\\r\
    \n    as-value where \\\r\
    \n       epoch > \$btOldestAdvertisementTimestamp and \\\r\
    \n       address ~ \$addressRegex and \\\r\
    \n       data ~ \$advertisingDataRegex \r\
    \n]\r\
    \n:local advJson \"\"\r\
    \n:local advCount 0\r\
    \n:local advSeparator \"\"\r\
    \n:local lastAdvTimestamp 0\r\
    \n\r\
    \n\r\
    \n:foreach adv in=\$advertisements do={\r\
    \n    :local address (\$adv->\"address\")\r\
    \n    :local ts (\$adv->\"epoch\")\r\
    \n    :local rssi (\$adv->\"rssi\")\r\
    \n    :local ad [:pick (\$adv->\"data\") 14 [:len (\$adv->\"data\")]]\r\
    \n    :local acrc [:tonum \"0x\$[:pick \$ad ([:len \$ad] - 2) [:len \$ad]]\
    \"]\r\
    \n    :local ccrc [:tonum [\$crc8 \$ad (([:len \$ad] - 2) / 2)]]\r\
    \n    :if ( \$acrc = \$ccrc ) do={\r\
    \n    \r\
    \n        :local fwver [:tonum \"0x\$[:pick \$ad 2 4]\"]\r\
    \n        :local hwver [:tonum \"0x\$[:pick \$ad 4 6]\"]\r\
    \n        :local lifec [:tonum \"0x\$[\$revb [:pick \$ad 6 14]]\"]\r\
    \n        :local inuse [:tonum \"0x\$[\$revb [:pick \$ad 14 22]]\"]\r\
    \n        :local active [:tonum \"0x\$[\$revb [:pick \$ad 22 30]]\"]\r\
    \n        :local temp [:tonum (\$sb (\"0x\$[:pick \$ad 30 32]\"))]\r\
    \n        :local vbat [:tonum \"0x\$[:pick \$ad 32 34]\"]\r\
    \n        :local batp [:tonum \"0x\$[:pick \$ad 34 36]\"]\r\
    \n        :local rpm [:tonum \"0x\$[\$revb [:pick \$ad 36 40]]\"]\r\
    \n        :local activity [:tonum \"0x\$[\$revb [:pick \$ad 40 44]]\"]\r\
    \n    \r\
    \n        :local obj \"\\\r\
    \n            {\\\r\
    \n                \\\"dev_mac\\\":\\\"\$address\\\",\\\r\
    \n                \\\"ts\\\":\$ts,\\\r\
    \n                \\\"rssi\\\":\$rssi,\\\r\
    \n                \\\"cnt1\\\":\$lifec,\\\r\
    \n                \\\"cnt2\\\":\$inuse,\\\r\
    \n                \\\"cnt3\\\":\$active,\\\r\
    \n                \\\"vbat\\\":\$vbat,\\\r\
    \n                \\\"charge\\\":\$batp,\\\r\
    \n                \\\"temp\\\":\$temp,\\\r\
    \n                \\\"fw_ver\\\":\$fwver,\\\r\
    \n                \\\"rpm\\\":\$rpm,\\\r\
    \n                \\\"activity\\\":\$activity,\\\r\
    \n                \\\"data\\\":\\\"\$ad\\\"\\\r\
    \n            }\"\r\
    \n        :set \$advCount (\$advCount + 1)\r\
    \n        :set \$lastAdvTimestamp \$ts\r\
    \n        # Ensure that the last object is not terminated by a comma\r\
    \n        :set \$advJson \"\$advJson\$advSeparator\$obj\"\r\
    \n        :if (\$advSeparator = \"\") do={ :set \$advSeparator \",\" }\r\
    \n    } else={\r\
    \n        :put \"[!] CRC error \$acrc!=\$ccrc (\$ad)\"\r\
    \n    }\r\
    \n}\r\
    \n\r\
    \n:if (\$advCount > 0) do={\r\
    \n    :set \$btOldestAdvertisementTimestamp \$lastAdvTimestamp\r\
    \n}\r\
    \n\r\
    \n:put (\"[*] Found \$advCount new advertisements \\\r\
    \n    (processing time: \$[([/system clock get time] - \$btProcessingStart\
    )])\")\r\
    \n\r\
    \n#################################### HTTP ##############################\
    #######\r\
    \n:local message \\\r\
    \n    \"{\\\r\
    \n        \\\"gw_mac\\\":\\\"\$ifaceMac\\\",\\\r\
    \n        \\\"ts\\\":\$(\$btOldestAdvertisementTimestamp / 1000),\\\r\
    \n        \\\"rssi\\\":0,\\\r\
    \n        \\\"sim_id\\\":\\\"xz\\\",\\\r\
    \n        \\\"model\\\":\\\"\$model\\\",\\\t\t\r\
    \n        \\\"sn\\\":\\\"\$serialNumber\\\",\\\r\
    \n        \\\"psu\\\":\$supplyVoltage,\\\r\
    \n        \\\"temp\\\":\$boardTemp,\\\t\t\r\
    \n        \\\"devs\\\":[\$advJson]\\\r\
    \n    }\"\r\
    \n:put (\"[*] Total message size: \$[:len \$message] bytes\")\r\
    \n/tool fetch http-method=post output=none http-header-field=\"Content-Typ\
    e:application/json\" http-data=\$message url=\"http://192.168.1.7/smm\"\r\
    \n:put (\"[*] Done\")"
add dont-require-permissions=no name=utils owner=admin policy=\
    read,write,policy,test source=":global crc8 do={\r\
    \n    :local crc 0\r\
    \n    :local crcar ( \"0x00\", \"0x5E\", \"0xBC\", \"0xE2\", \"0x61\", \"0\
    x3F\", \"0xDD\", \"0x83\", \"0xC2\", \"0x9C\", \"0x7E\", \"0x20\", \"0xA3\
    \", \"0xFD\", \"0x1F\", \"0x41\", \\\r\
    \n                        \"0x9D\", \"0xC3\", \"0x21\", \"0x7F\", \"0xFC\"\
    , \"0xA2\", \"0x40\", \"0x1E\", \"0x5F\", \"0x01\", \"0xE3\", \"0xBD\", \"\
    0x3E\", \"0x60\", \"0x82\", \"0xDC\", \\\r\
    \n                        \"0x23\", \"0x7D\", \"0x9F\", \"0xC1\", \"0x42\"\
    , \"0x1C\", \"0xFE\", \"0xA0\", \"0xE1\", \"0xBF\", \"0x5D\", \"0x03\", \"\
    0x80\", \"0xDE\", \"0x3C\", \"0x62\", \\\r\
    \n                        \"0xBE\", \"0xE0\", \"0x02\", \"0x5C\", \"0xDF\"\
    , \"0x81\", \"0x63\", \"0x3D\",\"0x7C\", \"0x22\", \"0xC0\", \"0x9E\", \"0\
    x1D\", \"0x43\", \"0xA1\", \"0xFF\", \\\r\
    \n                        \"0x46\", \"0x18\", \"0xFA\", \"0xA4\", \"0x27\"\
    , \"0x79\", \"0x9B\", \"0xC5\", \"0x84\", \"0xDA\", \"0x38\", \"0x66\", \"\
    0xE5\", \"0xBB\", \"0x59\", \"0x07\", \\\r\
    \n                        \"0xDB\", \"0x85\", \"0x67\", \"0x39\", \"0xBA\"\
    , \"0xE4\", \"0x06\", \"0x58\", \"0x19\", \"0x47\", \"0xA5\", \"0xFB\", \"\
    0x78\", \"0x26\", \"0xC4\", \"0x9A\", \\\r\
    \n                        \"0x65\", \"0x3B\", \"0xD9\", \"0x87\", \"0x04\"\
    , \"0x5A\", \"0xB8\", \"0xE6\", \"0xA7\", \"0xF9\", \"0x1B\", \"0x45\", \"\
    0xC6\", \"0x98\", \"0x7A\", \"0x24\", \\\r\
    \n                        \"0xF8\", \"0xA6\", \"0x44\", \"0x1A\", \"0x99\"\
    , \"0xC7\", \"0x25\", \"0x7B\", \"0x3A\", \"0x64\", \"0x86\", \"0xD8\", \"\
    0x5B\", \"0x05\", \"0xE7\", \"0xB9\", \\\r\
    \n                        \"0x8C\", \"0xD2\", \"0x30\", \"0x6E\", \"0xED\"\
    , \"0xB3\", \"0x51\", \"0x0F\", \"0x4E\", \"0x10\", \"0xF2\", \"0xAC\", \"\
    0x2F\", \"0x71\", \"0x93\", \"0xCD\", \\\r\
    \n                        \"0x11\", \"0x4F\", \"0xAD\", \"0xF3\", \"0x70\"\
    , \"0x2E\", \"0xCC\", \"0x92\", \"0xD3\", \"0x8D\", \"0x6F\", \"0x31\", \"\
    0xB2\", \"0xEC\", \"0x0E\", \"0x50\", \\\r\
    \n                        \"0xAF\", \"0xF1\", \"0x13\", \"0x4D\", \"0xCE\"\
    , \"0x90\", \"0x72\", \"0x2C\", \"0x6D\", \"0x33\", \"0xD1\", \"0x8F\", \"\
    0x0C\", \"0x52\", \"0xB0\", \"0xEE\", \\\r\
    \n                        \"0x32\", \"0x6C\", \"0x8E\", \"0xD0\", \"0x53\"\
    , \"0x0D\", \"0xEF\", \"0xB1\", \"0xF0\", \"0xAE\", \"0x4C\", \"0x12\", \"\
    0x91\", \"0xCF\", \"0x2D\", \"0x73\", \\\r\
    \n                        \"0xCA\", \"0x94\", \"0x76\", \"0x28\", \"0xAB\"\
    , \"0xF5\", \"0x17\", \"0x49\", \"0x08\", \"0x56\", \"0xB4\", \"0xEA\", \"\
    0x69\", \"0x37\", \"0xD5\", \"0x8B\", \\\r\
    \n                        \"0x57\", \"0x09\", \"0xEB\", \"0xB5\", \"0x36\"\
    , \"0x68\", \"0x8A\", \"0xD4\", \"0x95\", \"0xCB\", \"0x29\", \"0x77\", \"\
    0xF4\", \"0xAA\", \"0x48\", \"0x16\", \\\r\
    \n                        \"0xE9\", \"0xB7\", \"0x55\", \"0x0B\", \"0x88\"\
    , \"0xD6\", \"0x34\", \"0x6A\", \"0x2B\", \"0x75\", \"0x97\", \"0xC9\", \"\
    0x4A\", \"0x14\", \"0xF6\", \"0xA8\", \\\r\
    \n                        \"0x74\", \"0x2A\", \"0xC8\", \"0x96\", \"0x15\"\
    , \"0x4B\", \"0xA9\", \"0xF7\", \"0xB6\", \"0xE8\", \"0x0A\", \"0x54\", \"\
    0xD7\", \"0x89\", \"0x6B\", \"0x35\")\r\
    \n \r\
    \n    for i from=0 to=(\$2 - 1)  do={\r\
    \n        :local octet \"0x\$[:pick \$1 (\$i*2) (\$i*2 + 2)]\"\r\
    \n        set \$crc [:pick \$crcar ([:tonum \$octet] ^ [:tonum \$crc])]\r\
    \n    }\r\
    \n    :return \$crc\r\
    \n}\r\
    \n\r\
    \n:global revb do={\r\
    \n    :local out\r\
    \n    for i from=([:len \$1] / 2 - 1) to=0 do={\r\
    \n        :set \$out (\$out.[:pick \$1 (\$i * 2) (\$i * 2 + 2)])\r\
    \n    }\r\
    \n    :return \$out\r\
    \n}\r\
    \n\r\
    \n:global sb do={\r\
    \n    :local out\r\
    \n    :if ( \$1 > 127 ) do={ :set \$out ([:tonum \$1] - 256) } else={ :set\
    \_\$out \$1 }\r\
    \n    :return \$out\r\
    \n}"

/system scheduler
add interval=30s name=getTags on-event="/system script run get_tags \r\
    \n" policy=\
    ftp,reboot,read,write,policy,test,password,sniff,sensitive,romon \
    start-time=startup
